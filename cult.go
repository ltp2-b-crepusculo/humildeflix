package main

type CultMovie struct {

	Movie
}

func (CultMovie) String() (string) {
	return "Cult"
}

func (CultMovie) CollectRatings() (float32, error) {
	return 5, nil
}
