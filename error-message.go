package main

type HTTPMessage struct {
	Code int `json: "code"`
	Message string `json: "message"`
}

var HTTPMessages = map[int]HTTPMessage {
  418: HTTPMessage{Code: 418, Message: "Gender not found"},
  201: HTTPMessage{Code: 200, Message: "Object sucessfully created"},
}
