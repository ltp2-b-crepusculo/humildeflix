package main

type SuperheroMovie struct {

	Movie
}

func (SuperheroMovie) String() (string) {
	return "Super-Hero"
}

func (SuperheroMovie) CollectRatings() (float32, error) {
	return 8, nil
}
