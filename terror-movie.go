package main

type TerrorMovie struct {

	Movie
}

func (TerrorMovie) String() (string) {
	return "Terror"
}

func (TerrorMovie) CollectRatings() (float32, error) {
	return 10, nil
}
